
var datos = {
 "version" : 26052021,
 "datos" : [
   {  
   "front_end": [
        {
	  "titulo": "roadmap.sh - Arbol de aprendizaje",
	  "url": "https://roadmap.sh/frontend",
	  "descripcion": "[github:@luzxyz] Página de referencias para aprender front end en desarrollo web"
	},
	{
	  "titulo": "developer.mozilla.org - Documentacion de MDN",
	  "url":"https://developer.mozilla.org/en-US/docs/Web",
	  "descripcion": "[github:@luzxyz] Documentación dedicada de desarrollo front end de Mozilla"
	},
	{
	  "titulo": "Codepen.io - pruebas front end",
	  "url": "https://codepen.io/pen",
	  "descripcion":"[github:@luzxyz] Página para hacer pruebas front end"
	}
      ],
   "back_end": [
        {
         "titulo": "roadmap.sh - Arbol de aprendizaje backend web",
	 "url": "roadmap.sh/backend",
	 "descripcion": "[github:@luzxyz] Página de referencias para aprender back end en desarrollo web"
	}
      ],
   "server_dev_ops":[
        {
         "titulo": "roadmap.sh - Arbol de aprendizaje DevOps",
	 "url": "https://roadmap.sh/devops",
	 "descripcion": "[github:@luzxyz] Página de referencias para aprender de DevOps"
        },
	{
	 "titulo": "Hackthissite.org - practicar hacking etico",
	 "url": "https://www.hackthissite.org/",
	 "descripcion": "[github:@luzxyz] Foro / sitio web para entrenar habilidades de cracking"
	}
      ],
   "ia_analisis":[
        {
	   "titulo": "resarch.google - Google datasets",
	   "url": "https://research.google/tools/datasets/",
	   "descripcion": "[github:@luzxyz] Datasets de google"
	}
      ],
   "matematicas":[
        {
	   "titulo": "wolframalpha.com - servicio robusto de matemáticas",
	   "url": "https://www.wolframalpha.com/",
	   "descripcion": "[github:@luzxyz] Servicio web para evaluar procesos amplios de matemáticas"
	}
      ],
   "videojuegos":[
          {
	    "titulo": "itch.io - distribuir videojuegos",
	    "url": "https://itch.io",
  	    "descripcion": "[github:@luzxyz]Sitio web para distribución de videojuegos multimodal"
          }
        ],
   "otros": [
          {
	   "titulo": "terminal.sexy - personalizar UI en GNU/Linux",
	   "url": "terminal.sexy",
           "descripcion": "[github:@luzxyz] Página para diseño de terminales de GNU/Linux"
	  },
	  {
	  "titulo": "MIT open courseware",
	  "url": "https://www.youtube.com/user/MIT",
	  "descripcion": "[github:@luzxyz] cursos abiertos de varias asignaturas que sube el MIT"
	   },
	   {
	   "titulo": "JSlint.com - calidad de codigo JS",
	   "url": "https://jslint.com",
	   "descripcion": "[github:@luzxyz] Página para evaluar calidad de código javascript"
	   },
           {
	   "titulo": "jsonbin.io - Sitio de alojamiento gratuito datos JSON",
	   "url": "https://jsonbin.io/",
           "descripcion": "[github:@luzxyz] Página de alojamiento datos JSON"
	   }
      ]
  }]
}

